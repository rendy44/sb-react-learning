import './App.css';
import {useState} from "react";

function App() {
    const [name, setName] = useState('');
    const [isLoading, setIsLoading] = useState(false);
    const [result, setResult] = useState(0);
    const onSubmit = e => {
        // Prevent the browser from reloading the page
        e.preventDefault();
        setIsLoading(true)

        // Run simple API.
        fetch(`https://api.agify.io?name=${name}`)
            .then((response) => response.json())
            .then(data => {
                const {age} = data;
                // Save the result onto state.
                setResult(age);
                // Reset form.
                setIsLoading(false)
            })
    }
    const simpleStyle = {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        height: '100vh',
        flexDirection: 'column',
    };
    const formStyle = {
        display: 'flex',
        flexDirection: 'column',
        gap: '10px'
    }
    return <div style={simpleStyle}>
        {result ? <p>The age of ${name} is {result}</p> : <p>Write your name and we'll calculate your age</p>}
        <form onSubmit={onSubmit} style={formStyle}>
            <label>
                <input type={'text'} value={name} placeholder={'Put your name...'}
                       onChange={e => setName(e.target.value)}/>
            </label>
            <input type={'submit'} value={isLoading ? 'Calculating...' : 'Calculate'}/>
        </form>
    </div>
}

export default App;
